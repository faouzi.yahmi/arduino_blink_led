#include <Arduino.h>


void pin_start()
{
  DDRB |= 1;
  PORTB = PORTB & 0xfe;
  delay(2);
  DDRB = DDRB & 0xfe;
}

 
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
 
  }

void loop() {
  // put your main code here, to run repeatedly:
 
  pin_start();
  
  delay(10000);
}
