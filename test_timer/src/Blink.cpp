/*
 * Blink
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */

#include <Arduino.h>

void led_on()
{
  PORTB = PORTB | (1<<5);
}

void led_off()
{
  PORTB = PORTB &~(1<<5);
}
void init_port()
{
  DDRB = DDRB | (1<<5);
}
void led_toggle()
{
  PINB |= (1<<5);
}
void init_timer_1s()
{
  TCCR1A =0;
  TCCR1B = (1 << WGM12)| (1<<CS12) | (1<<CS10);
  OCR1A = 15625;
  TCNT1 = 0;
  TIMSK1 = (1<< OCIE1A);
}

ISR(TIMER1_COMP_vect)
{
  led_toggle();
}
void setup()
{
  // initialize LED digital pin as an output.
  //pinMode(LED_BUILTIN, OUTPUT);
  init_port();
  init_timer_1s();
}

void loop()
{
 
}
