/*
 * Blink
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */

#include <Arduino.h>

void led_on()
{
  PORTB = PORTB | (1<<5);
}

void led_off()
{
  PORTB = PORTB &~(1<<5);
}
void init_port()
{
  DDRB = DDRB | (1<<5);
}
void led_toggle()
{
  PINB |= (1<<5);
}

void setup()
{
  // initialize LED digital pin as an output.
  //pinMode(LED_BUILTIN, OUTPUT);
  init_port();

}

void loop()
{
 led_toggle();
 //led_on()
delay(1000);
 //led_off()
//delay(1000)
}
